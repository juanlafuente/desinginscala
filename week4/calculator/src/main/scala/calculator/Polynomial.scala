package calculator

object Polynomial {
  def computeDelta(a: Signal[Double], b: Signal[Double],
      c: Signal[Double]): Signal[Double] = {
    Signal(Math.pow(b(), 2) - 4*a()*c())
  }

  def computeSolutions(a: Signal[Double], b: Signal[Double],
      c: Signal[Double], delta: Signal[Double]): Signal[Set[Double]] = {
    def exp = {
      val _d = delta()
      val _b = b()
      val _a = a()
      if (_d>0) Set((-_b+Math.sqrt(_d))/2*_a, (-_b-Math.sqrt(_d))/2*_a)
      else Set(-_b/2*_a)
    }
    Signal(exp)
  }
}
