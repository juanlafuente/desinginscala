package quickcheck

import common._

import org.scalacheck._
import Arbitrary._
import Gen._
import Prop._
import Math._

abstract class QuickCheckHeap extends Properties("Heap") with IntHeap {

  lazy val genHeap: Gen[H] = {
    
    def nonEmptyHeap: Gen[H] = for {
      k <- arbitrary[A]
      heap <- genHeap
    } yield insert(k, heap)
    
    for {
      h <- oneOf(const(empty), nonEmptyHeap) 
    } yield h
  }
  
  implicit lazy val arbHeap: Arbitrary[H] = Arbitrary(genHeap)

  property("gen1") = forAll { (h: H) =>
    val m = if (isEmpty(h)) 0 else findMin(h)
    findMin(insert(m, h)) == m
  }
  
  property("gen2") = forAll { (e1: A, e2: A) =>
    min(e1,e2) == findMin(insert(e2, insert(e1, empty)))
  }
  
  property("gen3") = forAll { (e1: A) =>
    isEmpty(deleteMin(insert(e1, empty)))
  }
  
  property("gen4") = forAll { (h: H) =>
    def isSorted(h: H): Boolean =
      if (isEmpty(h)) true
      else {
        val m = findMin(h)
        val h2 = deleteMin(h)
        if (isEmpty(h2)) true
        else m <= findMin(h2) && isSorted(h2)
      }
    isSorted(h)
  }
  
  property("gen5") = forAll { (h1: H, h2: H) =>
    if (isEmpty(h1) || isEmpty(h2) ) true
    else {
      findMin(meld(h1,h2)) == min(findMin(h1), findMin(h2))
    }
  }
  
  property("gen6") = forAll { (h1: H, h2: H) =>
    def heapEqual(h1: H, h2: H): Boolean =
      if (isEmpty(h1) && isEmpty(h2)) true
      else {
        val m1 = findMin(h1)
        val m2 = findMin(h2)
        m1 == m2 && heapEqual(deleteMin(h1), deleteMin(h2))
      }
    if (isEmpty(h1) || isEmpty(h2)) true
    else 
      heapEqual(meld(h1, h2),
              meld(deleteMin(h1), insert(findMin(h1), h2)))
  }
  
  property("gen7") = forAll { (h1: H, h2: H) =>
    if (isEmpty(h1) || isEmpty(h2)) true
    else {
      val m = min(findMin(h1), findMin(h2))
      findMin(meld(deleteMin(h1), insert(m, h2))) == m
    }
  } 

}
