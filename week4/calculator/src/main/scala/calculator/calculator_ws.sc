package calculator

object calculator_ws {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  val namedExpressions: Map[String, Signal[Expr]] =
      Map(
       "a" -> Signal(Minus(Literal(2.0),Literal(3.0))),
       "b" -> Signal(Plus(Ref("b"),Literal(3.0))),
       "c" -> Signal(Plus(Ref("b"),Literal(3.0)))
       )                                          //> namedExpressions  : Map[String,calculator.Signal[calculator.Expr]] = Map(a -
                                                  //| > calculator.Signal@48eff760, b -> calculator.Signal@402f32ff, c -> calculat
                                                  //| or.Signal@573f2bb1)
  
  Calculator.computeValues(namedExpressions)      //> res0: Map[String,calculator.Signal[Double]] = Map(a -> calculator.Signal@3a8
                                                  //| 2f6ef, b -> calculator.Signal@100fc185, c -> calculator.Signal@643b1d11)
  
}