package calculator

object calculator_ws {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(86); 
  println("Welcome to the Scala worksheet");$skip(232); 
  
  val namedExpressions: Map[String, Signal[Expr]] =
      Map(
       "a" -> Signal(Minus(Literal(2.0),Literal(3.0))),
       "b" -> Signal(Plus(Ref("b"),Literal(3.0))),
       "c" -> Signal(Plus(Ref("b"),Literal(3.0)))
       );System.out.println("""namedExpressions  : Map[String,calculator.Signal[calculator.Expr]] = """ + $show(namedExpressions ));$skip(48); val res$0 = 
  
  Calculator.computeValues(namedExpressions);System.out.println("""res0: Map[String,calculator.Signal[Double]] = """ + $show(res$0))}
  
}
