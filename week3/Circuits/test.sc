package week3.circuits

object test {
  
  object sim extends Circuits with Parameters
  import sim._
  
  val in1, in2, sum, carry = new Wire             //> in1  : week3.circuits.test.sim.Wire = false
                                                  //| in2  : week3.circuits.test.sim.Wire = false
                                                  //| sum  : week3.circuits.test.sim.Wire = false
                                                  //| carry  : week3.circuits.test.sim.Wire = false
	probe("sum", sum)                         //> sum 0 value = false
  probe("carry", carry)                           //> carry 0 value = false
  
  halfAdder(in1,in2,sum, carry)
  
  in1 setSignal true
  in2 setSignal true
  run()                                           //> *** simulation stated, time = 0 ***
                                                  //| carry 3 value = true
                                                  //| sum 8 value = true
                                                  //| sum 8 value = false

  in2 setSignal false
  in1 setSignal false
  run()                                           //> *** simulation stated, time = 8 ***
                                                  //| carry 11 value = false

  in1 setSignal true
  run()                                           //> *** simulation stated, time = 16 ***
                                                  //| sum 24 value = true
  in1 setSignal false
  run()                                           //> *** simulation stated, time = 24 ***
                                                  //| sum 32 value = false
}