package quickcheck

import org.scalatest.FunSuite
import org.scalacheck.Prop.forAll

import org.scalatest.prop.Checkers
import org.scalacheck.Arbitrary._
import org.scalacheck.Prop
import org.scalacheck.Prop._

object testCheck extends FunSuite with Checkers {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  
  
  val q = new QuickCheckHeap with quickcheck.test.BinomialHeap
                                                  //> q  : quickcheck.QuickCheckHeap with quickcheck.test.BinomialHeap = Prop
  
  check(q)
  
}